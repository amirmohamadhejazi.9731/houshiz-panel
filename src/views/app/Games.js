import React from 'react';
import { Row } from 'reactstrap';
import { Colxx, Separator } from '../../components/common/CustomBootstrap';

import imgGame from "../../assets/img/saints.jpg"
import {NavLink} from "react-router-dom";
const Games = ({ match }) => {
    return (
        <>
            <Row>
                <Colxx xxs="12" className="mb-4  ">
                    <div className="overflow-hidden" style={{width:"500px" , height:"300px" ,borderRadius: "34px"}}>
                        <img src={imgGame} width="100%" height="100%" alt="imgGame"/>
                    </div>
                </Colxx>
            </Row>
        </>
    );
};

export default Games;
