import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from '../../layout/AppLayout';
// import { ProtectedRoute, UserRole } from '../../helpers/authHelper';

const Contacts = React.lazy(() =>
  import(/* webpackChunkName: "viwes-gogo" */ './Contacts')
);


const MainCategory = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './categories/MainCategory')
);
const SubCategories = React.lazy(() =>
  import(/* webpackChunkName: "viwes-second-menu" */ './categories/SubCategories')
);


const Questions = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ './Questions')
);
const TypesGames = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ './TypesGames')
);
const Games = React.lazy(() =>
  import(/* webpackChunkName: "viwes-blank-page" */ './Games')
);
const DailyGifts = React.lazy(() =>
    import(/* webpackChunkName: "viwes-blank-page" */ './DailyGifts')
);

const App = ({ match }) => {
  return (
    <AppLayout>
      <div className="dashboard-wrapper">
        <Suspense fallback={<div className="loading" />}>
          <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/contacts`} />
            <Route
              path={`${match.url}/contacts`}
              render={(props) => <Contacts {...props} />}
            />
            <Route
                path={`${match.url}/categories/mainCategory`}
                render={(props) => <MainCategory {...props} />}
            />
            <Route
                path={`${match.url}/categories/subCategories`}
                render={(props) => <SubCategories {...props} />}
            />
            <Route
                path={`${match.url}/questions`}
                render={(props) => <Questions {...props} />}
            />
            <Route
                path={`${match.url}/games`}
                render={(props) => <Games {...props} />}
            />
            <Route
                path={`${match.url}/typesGames`}
                render={(props) => <TypesGames {...props} />}
            />
            <Route
              path={`${match.url}/dailyGifts`}
              render={(props) => <DailyGifts {...props} />}
            />
            <Redirect to="/error" />
          </Switch>
        </Suspense>
      </div>
    </AppLayout>
  );
};

const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(App));
