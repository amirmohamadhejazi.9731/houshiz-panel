import { adminRoot } from "./defaultValues";
// import { UserRole } from "../helpers/authHelper"

const data = [
  {
    id: 'Contacts',
    icon: 'simple-icon-user',
    label: 'مخاطبین',
    to: `${adminRoot}/contacts`,
  },
  {
    id: 'Grouping',
    icon: 'iconsminds-three-arrow-fork',
    label: 'دسته بندی ها',
    to: `${adminRoot}/categories`,
    subs: [
      {
        icon: 'simple-icon-layers',
        label: 'دسته بندی اصلی',
        to: `${adminRoot}/categories/mainCategory`,
      },
      {
        icon: 'simple-icon-list',
        label: 'زیر دسته بندی ها',
        to: `${adminRoot}/categories/subCategories`,
      },
    ],
  },
  {
    id: 'question',
    icon: 'simple-icon-question',
    label: 'سوالات',
    to: `${adminRoot}/questions`,
  },
  {
    id: 'TypesGames',
    icon: 'iconsminds-gamepad-2',
    label: 'انواع بازی‌ ها',
    to: `${adminRoot}/typesGames`,
  },
  {
    id: 'games',
    icon: 'simple-icon-game-controller',
    label: 'بازی ها',
    to: `${adminRoot}/games`,
  },
  {
    id: 'dailyGifts',
    icon: 'iconsminds-gift-box',
    label: 'پکیج جایزه‌ی روزانه',
    to: `${adminRoot}/dailyGifts`,
  },
];
export default data;
