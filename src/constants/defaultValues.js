import React from "react";
import { UserRole } from "../helpers/authHelper";

/*
Menu Types:
"menu-default", "menu-sub-hidden", "menu-hidden"
*/
export const defaultMenuType = 'menu-default';

export const subHiddenBreakpoint = 1440;
export const menuHiddenBreakpoint = 768;
export const defaultLocale = 'en';
export const localeOptions = [
  { id: 'en', name: 'English - LTR', direction: 'ltr' },
  { id: 'es', name: 'Español', direction: 'ltr' },
  { id: 'enrtl', name: 'English - RTL', direction: 'rtl' },
];

export const firebaseConfig = {
  apiKey: 'AIzaSyBBksq-Asxq2M4Ot-75X19IyrEYJqNBPcg',
  // authDomain: 'gogo-react-login.firebaseapp.com',
  databaseURL: 'https://gogo-react-login.firebaseio.com',
  // databaseURL: 'http://195.201.73.13:8080/api/v1/admins/auth/login',
  // projectId: 'gogo-react-login',
  // storageBucket: 'gogo-react-login.appspot.com',
  messagingSenderId: '216495999563',
};

export const adminRoot = '/app';
export const buyUrl = 'https://1.envato.market/k4z0';
export const searchPath = `${adminRoot}/#`;
export const servicePath = 'https://api.coloredstrategies.com';

export const currentUser = {
  id: 1,
  title: 'Sarah Kortney',
  img: '/assets/img/profiles/l-11.jpg',
  date: 'Last seen today 15:24',
  role: UserRole.Admin
}


export const themeColorStorageKey = '__theme_selected_color';
export const isMultiColorActive = true;
export const defaultColor = 'light.orangecarrot';
export const isDarkSwitchActive = true;
export const defaultDirection = 'rtl';
export const themeRadiusStorageKey = '__theme_radius';
export const isAuthGuardActive = true;
export const colors = [
  'bluenavy',
  'blueyale',
  'blueolympic',
  'greenmoss',
  'greenlime',
  'purplemonster',
  'orangecarrot',
  'redruby',
  'yellowgranola',
  'greysteel',
];












// ******************************


// export const baseURL = "https://houshiz.tk/api/v1";
export const baseURL = "http://195.201.73.13:8080/api/v1";
export const pageName = {
  index: "/",
  signUp: "/SIgnUp",
  dashboard: "/dashboard",
  users: { list: "/users", details: "/user/:id" },
  game: "/game",
  gameMode: { index: "/gameMode", add: "/gameMode/add" },
  question: {
    index: "/questions",
    add: "/questions/add",
  },
  category: {
    index: "/category",
    subCategory: "/subCategory",
  },
  daily: "/daily",
};

export const rules = {
  required: { required: true, message: "الزامی" },
  min: (min) => ({ min, message: `باید حداقل ${min} کاراکتر باشد.` }),
  max: (max) => ({ max, message: `باید حداکثر ${max} کاراکتر باشد.` }),
  len: (len) => ({ len: len, message: `باید ${len} کاراکتر باشد` }),
  type: (type) => ({ type, message: `فرمت وارد شده اشتباه است` }),
  number: (min, max) => ({
    type: "number",
    min,
    max,
    message: "خطا در ثبت اطلاعات",
  }),
};

export const endPoints = {
  auth: {
    login: "/admins/auth/login",
    register: "/admins/auth/register",
    checkCode: "/admins/auth/checkcode",
    logOut: "/admins/auth/logout",
    me: "/admins/auth/me",
  },
  user: { list: "/admins/users/all", details: "/admins/users/getone" },
  subCategory: {
    getList: "/admins/subcategories/getall",
    add: "/admins/subcategories/add",
    update: "/admins/subcategories/update",
    delete: "/admins/subcategories/delete",
  },
  category: {
    getList: "/admins/categories/getall",
    add: "/admins/categories/add",
    update: "/admins/categories/update",
    delete: "/admins/categories/delete",
  },
  questoins: {
    getList: "/admins/questions/getall",
    getOne: "/admins/questions/getone",
    add: "/admins/questions/add",
    adds: "/admins/questions/adds",
    update: "/admins/questions/update",
    delete: "/admins/questions/delete",
  },
  gameMode: {
    getAll: "/admins/gamemode/getall",
    create: "/admins/gamemode/add",
    getOne: "/admins/gamemode/getone",
    update: "/admins/gamemode/update",
    delete: "/admins/gamemode/delete",
  },
  daily: {
    list: "/admins/daily/getall",
    add: "/admins/daily/add",
    update: "/admins/daily/update",
    delete: "/admins/daily/delete",
  },
  game: {
    list: "/admins/games/getall",
    delete: "/admins/games/delete",
  },
};

export const messages = {
  successfull: "با موفقیت انجام شد",
  created: "با موفقیت ساخته شد",
  deleted: "با موفقیت حذف شد",
  updated: "با موفقیت ویرایش شد",
  rightAnswerCount: "تعدا سوالات صحیح پاسخ داده شده",
};

export const sideBarMenu = [
  { name: "مخاطبین", route: pageName.users.list},
  {
    name: "دسته بندی ها",
    children: [
      {
        name: "دسته بندی اصلی",
        route: pageName.category.index,
      },
      {
        name: "زیر دسته بندی ها",
        route: pageName.category.subCategory,
      },
    ],
  },
  {
    name: "سوالات",
    route: pageName.question.index,
  },
  {
    name: "انواع بازی",
    route: pageName.gameMode.index,
  },
  {
    name: "بازی ها",
    route: pageName.game,
  },
  {
    name: "پکیج جایزه روزانه",
    route: pageName.daily,
  },
];
